<?php

use App\Advert;
use Illuminate\Support\Facades\Route;
use App\User;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('users', 'UserController');



Auth::routes();


Route::get('users/{user}', 'UserController@show')->name('users.show');


Route::group(['middleware'=>'auth'],function(){

    //Advert routes
    Route::get('adverts/index/{start?}/{end?}', function ($start = null, $end=null) {
        //Get collection of adverts
        if($start == null){
            $start = Carbon\Carbon::now()->startOfDay();
            $end = Carbon\Carbon::now()->endOfDay();
        }
        $adverts = Advert::whereBetween('created_at', [$start,$end])->get();
        $user = Auth::user();
        return view('advert.index', compact('adverts','user'));
    })
        ->name('adverts.index');
    Route::get('/', function () {
        return redirect()->route('adverts.index');
    });
    Route::get('adverts/store', 'AdvertsController@store')
        ->name('adverts.store');
    Route::delete('adverts/{advert}', 'AdvertsController@destroy')
        ->name('adverts.destroy');

    //Comment routes
    Route::get('comments/{user}', 'CommentController@show')
        ->name('comments.show');
    Route::post('comments/{advert}', 'CommentController@store')
        ->name('comments.store');

    //Group routes
    Route::get('groups/{group}', 'GroupController@show')
        ->name('groups.show');
    Route::post('groups', 'GroupController@store')
        ->name('groups.store');

    //AdvertGroup routes
    Route::post('advert_group/','AdvertGroupController@store')
        ->name('advert_group.store');

    //Role routes
    Route::get('roles/{role}', 'RoleController@show')
        ->name('roles.show');

    //User routes
    Route::get('users/{user}/edit','UserController@edit')
        ->name('users.edit');
    Route::patch('users/{user}', 'UserController@update')
        ->name('users.update');
    Route::delete('user/{user}', 'userController@destroy')
        ->name('user.destroy');

    //User image routes
    Route::post('user_images/{user}', 'UserImageController@store')
        ->name('user_images.store');
    Route::patch('user_images/{user}', 'UserImageController@update')
        ->name('user_images.update');
    Route::delete('user_images/{user}', 'UserImageController@destroy')
        ->name('user_images.destroy');
});
