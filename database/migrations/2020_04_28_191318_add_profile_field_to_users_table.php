<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddProfileFieldToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('surname');
	        $table->string('image')->nullable();
	        $table->date('date_of_birth')->nullable();
	        $table->string('bio',500)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
	        $table->dropColumn('surname');
	        $table->dropColumn('image');
	        $table->dropColumn('date_of_birth');
	        $table->dropColumn('bio');
        });
    }
}
