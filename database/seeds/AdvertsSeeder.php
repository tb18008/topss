<?php

use Illuminate\Database\Seeder;

class AdvertsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('adverts')->insert([
            'title' => 'Home for sale',
            'pub_date' => '2020-05-27',
            'description' => 'Special offer',
            'link' => 'https://www.google.com/'
        ]);
    }
}
