<?php

use Illuminate\Database\Seeder;

class Advert_GroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('advert_group')->insert([
            'advert_id' => 1,
            'group_id' => 1,
        ]);
    }
}
