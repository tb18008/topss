<?php

use Illuminate\Database\Seeder;

class CommentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('comments')->insert([
            'advert_id' => 1,
            'user_id' => 1,
            'text' => 'Abc',
        ]);
    }
}
