<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Kārlis',
	        'surname' => 'Bērziņš',
            'email' => 'topss@gmail.com',
            'password' => Hash::make('password'),
        ]);
	    DB::table('users')->insert([
            'name' => 'Jānis',
	        'surname' => 'Liepiņš',
            'email' => 'topss1@gmail.com',
            'password' => Hash::make('password'),
        ]);
    }
}
