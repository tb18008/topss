<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersSeeder::class);
        $this->call(RolesSeeder::class);
        $this->call(Role_UserSeeder::class);
        $this->call(AdvertsSeeder::class);
        $this->call(CommentsSeeder::class);
        $this->call(GroupSeeder::class);
        $this->call(Advert_GroupSeeder::class);
    }
}
