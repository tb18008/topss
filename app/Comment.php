<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{

    protected $fillable = [
        'text',
        'user_id'
    ];

    public function advert()
    {
        return $this->belongsTo('App\Advert');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
