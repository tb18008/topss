<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdvertGroupStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'group_id' => 'required|exists:groups,id|
            unique:advert_group,group_id,NULL,id,advert_id,'.$this->advert_id,
            //group_id and advert_id are unique to the table group_adverts
            'advert_id' => 'required|exists:adverts,id',
        ];
    }
    public function messages()
    {
        return [
            'unique' => 'This advert already exists in group',
        ];
    }
}
