<?php

namespace App\Http\Controllers;

use App\Advert;
use App\User;
use Illuminate\Http\Request;

class AdvertsController extends Controller
{
    public function store()
    {
        //Get array of adverts from xml document
        $xml = file_get_contents("https://www.ss.com/lv/real-estate/flats/riga/centre/rss/");
        $root = new \SimpleXMLElement($xml);
        $items = $root->channel[0]->{'item'};
        foreach ($items as $item) {
            $advert = new Advert;
            $advert->title = $item->title;
            $advert->description = $item->description;
            $advert->pub_date = date("Y-m-d H:i:s", strtotime($item->pubDate));
            $advert->link = $item->link;
            $advert->save();
        }
        return back();
    }
    public function destroy(Advert $advert)
    {
        $this->authorize('delete', $advert);
        Advert::destroy($advert->id);
        return redirect('/');
    }
}
