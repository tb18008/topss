<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserImagePostRequest;
use App\ImageService;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UserImageController extends Controller
{
    public function store(UserImagePostRequest $request, User $user)
    {
        $image_service = new ImageService($request->validated()->image);

        $resized_image = $image_service->resize();  //Use the image service's resize function
        $save_img_name = 'image'.date_timestamp_get(date_create()).'.jpg';	//Make image name
        Storage::disk('local')->put('public/'.$save_img_name, (string) $resized_image->encode());    //Store the resized image at /storage/app
        $user->image = $save_img_name;  //Assign new value to the user
        $user->save();
        return redirect('/users/'.$user->id)->with(['user'=>$user]);
    }
    public function update(UserImagePostRequest $request, User $user)
    {
        $image_service = new ImageService($request->validated()->image);

        $resized_image = $image_service->resize();  //Use the image service's resize function
        //Overwrite the image at the previous file path
        Storage::disk('local')->put('public/'.$user->image, (string) $resized_image->encode());    //Store the resized image at /storage/app

        return redirect('/users/'.$user->id)->with(['user'=>$user]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        Storage::delete('public/'.$user->image);
        $user->image = null;
        $user->save();
        return redirect('/users/'.$user->id)->with(['user'=>$user]);
    }
}
