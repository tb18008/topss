<?php

namespace App\Http\Controllers;

use App\Advert;
use App\AdvertGroup;
use App\Group;
use App\Http\Requests\AdvertGroupStoreRequest;

class AdvertGroupController extends Controller
{
    public function store(AdvertGroupStoreRequest $request){
        $group = Group::where('id',$request->validated()['group_id'])->first();
        $advert = Advert::where('id',$request->validated()['advert_id'])->first();
        $group->adverts()->save($advert);
        return back();
    }
}
