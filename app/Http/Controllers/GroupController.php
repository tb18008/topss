<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\GroupStoreRequest;
use App\Group;

class GroupController extends Controller
{
    public function store(GroupStoreRequest $request){
        $this->authorize('create', [Group::class, $request->user_id]);
        $group = new Group;
        $group->create($request->validated());
        return back();
    }
    public function show(Group $group){
        $group = new Group;
        $group->create($request->validated());
        return back();
    }
}
