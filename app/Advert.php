<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Advert extends Model
{
    protected $fillable = ['title', 'pub_date', 'description', 'link'];

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }
    public function groups()
    {
        return $this->belongsToMany('App\Group');
    }

    use SoftDeletes;
}
