<?php

namespace App;

use Intervention\Image\Facades\Image as Image;

class ImageService
{
    public $image;

    /**
     * ImageService constructor.
     * @param $image
     */
    public function __construct($image)
    {
        $this->image = $image;
    }

    public function resize($width = 50, $height = 50){

        //Image resizing
        return Image::make($this->image)
            ->resize($width, $height, function($constraint) {
               $constraint->aspectRatio();
            });

    }
}
