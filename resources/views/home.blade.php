@extends('layouts.app')

@section('content')
<!--Lapa, kas satur sarakstu ar publikācijām. Skatu iegūt var HomeController@index. Izmanto masīvu $adverts ar publikācijām-->
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">@lang('messages.adverts')</div> <!--atsaucas uz \resources\lang\{locale}\messages, kas satur masīvu. Piemēram, masīva atslēga ir 'adverts' un latviešu lokalizācijā atgriež 'Publikācijas'-->

                <div class="card-body">                    
					<div class="card-text">
					<table class="table table-hover">
					<thead>
						<tr class="table-primary">
							<th scope="col">Apraksts</th>
							<th scope="col">Publicēšanas datums</th>
							<th scope="col">Sludinājums</th>
						</tr>
					</thead>
					@foreach($adverts as $advert)
						<tr>
							<td>{!! $advert->description !!}</td>	<!--Publikācijas apraksts satur jaunu līniju birkas un tiek lasīts kā multilīnijas teksts-->
							<td>{{ date('d.m.yy', strtotime($advert->pubDate)) }}</td> <!--Noformēts datums  date(lauka aina, laika zīmogs)-->
							<td>{{ $advert->title }}</td>			
						</tr>
					@endforeach
					</table>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
