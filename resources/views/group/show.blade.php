@inject('user', 'App\User')
<div class="card" >
    <div class="card-body">
        <h5 class="card-title">{{ $group->name }}</h5>
        <h6 class="card-subtitle mb-2 text-muted">{{$user->find($group->user_id)->fullName()}}</h6>
        <ul>
            @foreach($group->adverts as $advert)
                <li>
                    <a href="{{$advert->link}}" class="card-link">{{$advert->title}}</a>
                </li>
            @endforeach
        </ul>
    </div>
</div>
