<form method="POST" action="{{ route('groups.store') }}">
    @csrf
    <div class="form-group row">
        <label for="name" class="col-md-2 col-form-label text-md-right">@lang('messages.name')</label>

        <div class="col-md-8">
            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{old('name')}}" required autocomplete="name" autofocus>
            @error('name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
            <input name="user_id" type="hidden" value="{{$user->id}}">
        </div>
        <button type="submit" class="btn btn-primary">
            @lang('messages.add')
        </button>
    </div>

</form>
