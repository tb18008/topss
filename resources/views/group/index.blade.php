<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">@lang('messages.groups')</div>
                @foreach($user->groups as $group)
                    @include('group.show')
                @endforeach
                @include('group.create')
            </div>
        </div>
    </div>
</div>
