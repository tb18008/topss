<form method="POST" action="{{ route('user_images.destroy', ['user'=>Auth::user()]) }}" >
    @method('DELETE')
    @csrf
    <button type="submit" class="btn btn-primary">
        @lang('messages.remove_img')
    </button>
</form>
