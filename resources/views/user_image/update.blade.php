@include('user_image.show')
<form method="POST" action="{{ route('user_images.update', ['user'=>Auth::user()]) }}" enctype="multipart/form-data">
    @method('PATCH')
    @csrf
    <div class="form-group row">
        <label for="image" class="col-md-4 col-form-label text-md-right">@lang('messages.image')</label>
        <div class="col-md-6">
            <!-- MAX_FILE_SIZE must precede the file input field -->
            <input type="hidden" name="MAX_FILE_SIZE" value="2000000" />
            <input id="image_input" type="file" class="form-control @error('image') is-invalid @enderror" name="image" value="{{ Auth::user()->image }}">
            @error('image')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
            <button type="submit" class="btn btn-primary">
                @lang('messages.save')
            </button>
        </div>
    </div>
</form>
