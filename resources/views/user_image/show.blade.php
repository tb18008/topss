<!--If user has no image then show alternate image-->
@if($user->image != null)
    <img src="{{url(Storage::url($user->image)) }}" alt="a"/>
@else
    <img src="{{url(Storage::url('image_blank.jpg')) }}"/>
@endif
