<div class="card" >
    <div class="card-body">
        <div class="row">
            <div class="col-10">
                {{--Advert info--}}
                <h5 class="card-title">{{ $advert->title }}</h5>
                <h6 class="card-subtitle mb-2 text-muted">{{$advert->pub_date}}</h6>
                <p class="card-text">{!! $advert->description !!}</p> <!--Advert description contains line breaks and images-->
                <a href="{{ $advert->link }}" class="card-link">{{ $advert->link }}</a>

            </div>
            <div class="col-2">
                @include('advert.destroy')
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                {{--Links to store advert in group adverts--}}
                @if(Auth::check())
                    @foreach($user->groups as $group)
                        @include('advert_group.create')
                    @endforeach
                    {{--Create comment--}}
                    @include('comment.create',['id'=>$advert->id])
                @endif
                {{--Comments--}}
                @foreach($advert->comments as $comment)
                    @include('comment.show')
                @endforeach
            </div>
        </div>
    </div>
</div>
