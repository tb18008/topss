@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">@lang('messages.adverts')</div> <!--references \resources\lang\{locale}\messages-->
            @foreach($adverts as $advert)
                @include('advert.show')
            @endforeach
        </div>
    </div>
@endsection
