@can('delete', $advert){{-- @cannot--}}
    <form method="POST" action="{{ route('adverts.destroy', ['advert'=>$advert]) }}" >
        @method('DELETE')
        @csrf
        <button type="submit" class="btn btn-danger">
            Delete advert
        </button>
    </form>
@endcan
