@inject('user', 'App\User')
@if($user->find($comment->user_id))
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">{{$user->find($comment->user_id)->name}}</h5>
            {{$comment->text}}
        </div>
    </div>
@endif
