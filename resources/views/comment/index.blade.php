@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">@lang('messages.comment')</div>
                @foreach($comments as $comment)
                    <div class="card" >
                        <div class="card-body">
                            <p class="card-text">{{$comment->text}}</p>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection
