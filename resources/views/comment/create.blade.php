<form method="POST" action="{{ route('comments.store', ['advert'=>$advert]) }}">
    @csrf
    <div class="form-group row">
        <label for="text" class="col-md-2 col-form-label text-md-right">@lang('messages.comment')</label>

        <div class="col-md-8">
            <input id="text" type="text" class="form-control @error('text') is-invalid @enderror" name="text" value="" required autocomplete="text" autofocus>
            @error('text')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
            <input name="user_id" type="hidden" value="{{Auth::user()->id}}">
        </div>
        <button type="submit" class="btn btn-primary">
            @lang('messages.save')
        </button>
    </div>
</form>

