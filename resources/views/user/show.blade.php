@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col">
            <div class="text-white bg-primary  card-header">
                @include('user_image.show')
                @lang('messages.user'): {{$user->name}} {{$user->surname}}
            </div>
            <div class="card bg-light">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">@lang('messages.e-mail'): {{$user->email}}</li>
                    <li class="list-group-item">@lang('messages.roles'):
                        <br>
                        @foreach($user->roles as $role)
                            {{$role->name}}<br>
                        @endforeach
                    </li>
                    @if ($user->date_of_birth != null)
                        <li class="list-group-item">@lang('messages.date_of_birth'): {{ date('d.m.Y', strtotime($user->date_of_birth)) }}</li>
                    @endif
                    <li class="list-group-item">@lang('messages.bio'): <br>{!!nl2br($user->bio)!!}</li>
                </ul>
                @if (Auth::check()&&$user->id == Auth::user()->id)
                    <a type="button" href="{{route('users.edit', ['user'=>Auth::user()])}}" class="btn btn-secondary">
                        @lang('messages.edit')
                    </a>
                @endif
            </div>
        </div>
        <div class="col">
            @include('group.index')
        </div>
    </div>
@endsection
