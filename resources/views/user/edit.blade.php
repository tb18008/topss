@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">@lang('messages.edit') {{Auth::user()->name}} {{Auth::user()->surname}}</div>
                <div class="card-body">
					<form method="POST" action="{{ route('users.update', ['user'=>Auth::user()]) }}">

                        @csrf
                        @method('patch')

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">@lang('messages.name')</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ Auth::user()->name }}" required autocomplete="name" autofocus>
								@error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

						<div class="form-group row">
                            <label for="surname" class="col-md-4 col-form-label text-md-right">@lang('messages.surname')</label>

                            <div class="col-md-6">
                                <input id="surname" type="text" class="form-control @error('surname') is-invalid @enderror" name="surname" value="{{ Auth::user()->surname }}" required autocomplete="surname" autofocus>

                                @error('surname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">@lang('messages.e-mail')</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ Auth::user()->email }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

						<div class="form-group row">
                            <label for="date_of_birth" class="col-md-4 col-form-label text-md-right">@lang('messages.date_of_birth')</label>

                            <div class="col-md-6">
								@if(Auth::user()->date_of_birth!=null)
									<!--Lietotājam ir iestatīts datums, parāda esošo vērtību-->
									<input type="date" id="date_of_birth" name="date_of_birth" max="{{date('Y-m-d')}}" value="{{ Auth::user()->date_of_birth }}"/>
								@else
									<!--Lietotājam nav iestatīts datums, nerāda vērtību-->
									<input type="date" id="date_of_birth" name="date_of_birth" max="{{date('Y-m-d')}}" value=""/>
								@endif
                                @error('date_of_birth')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

						<div class="form-group row">
                            <label for="bio" class="col-md-4 col-form-label text-md-right">@lang('messages.bio')</label>

                            <div class="col-md-6">
                                <textarea id="bio" class="form-control @error('bio') is-invalid @enderror" name="bio" >{{ Auth::user()->bio }}</textarea>

                                @error('bio')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    @lang('messages.save')
                                </button>
                            </div>
                        </div>
                    </form>
                    <!-- Shows form for storing an image if user doesn't have one -->
                    @if($user->image == null)
                        @include('user_image.store')
                    @else
                    <!-- Shows form for updating an image if user already has one -->
                        @include('user_image.update')
                        @include('user_image.destroy')
                    @endif
                    <form method="POST" action="{{ route('user.destroy', ['user'=>Auth::user()]) }}" >
                        @method('DELETE')
                        @csrf
                        <button type="submit" class="btn btn-primary">
                            @lang('messages.remove_user')
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
