@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">@lang('messages.users_with_role')</div>
                @foreach($users as $user)
                    <div class="card" >
                        <div class="card-body">
                            @include('user_image.show')
                            <h5 class="card-title">{{$user->name.' '.$user->surname}}</h5>
                            <h6 class="card-subtitle mb-2 text-muted">{{$user->date_of_birth}}</h6>
                            <p class="card-text">{!!$user->bio!!}</p>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection
