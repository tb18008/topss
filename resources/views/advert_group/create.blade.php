<form method="POST" action="{{ route('advert_group.store', ['group'=>$group,'advert'=>$advert]) }}">
    @csrf

    <input name="group_id" class="form-control @error('group_id') is-invalid @enderror" type="hidden" value="{{$group->id}}">
    @error('group_id')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
    <input name="advert_id" type="hidden" value="{{$advert->id}}">


    <button type="submit" class="btn btn-primary">
        Add advert to group {{$group->name}}
    </button>
</form>

